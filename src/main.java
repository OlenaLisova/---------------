

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {

    public static void main (String[] args) {

        Double res = null;
        String math = null;

        System.out.println("print what you need to calculate step by step, press enter after every number or mathematical symbol. Print 'clear' to erase all. Print 'exit' to close program.");
// open up standard input
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String symbol = null;
        Double digit = null;

        while(true) {
            try {
                symbol = br.readLine();
                if(symbol.equals("+") || symbol.equals("-") || symbol.equals("*") || symbol.equals("/")){
                    if(res == null){
                        System.err.println("need to print digit now");
                    } else {
                        math = symbol;
                    }
                } else if (symbol.equals("clear")) {
                    System.out.println("cleared!");
                    symbol = null;
                    digit = null;
                    res = null;
                } else if (symbol.equals("exit")) {
                    System.out.println("exit!");
                    System.exit(0);
                } else {
                    if (math == null && res != null){
                        System.err.println("need to print action now");
                    } else {
                        digit = Double.parseDouble(symbol);
                        if (res == null){
                            res = digit;
                        } else {
                            if(math.equals("+")){
                                res = res + digit;
                            }
                            if(math.equals("-")){
                                res = res - digit;
                            }
                            if(math.equals("*")){
                                res = res * digit;
                            }
                            if(math.equals("/")){
                                res = res / digit;
                            }
                            math = null;
                        }
                    }
                    System.out.println("res = " + res);

                }
            } catch (IOException e) {
                System.err.println("IO error trying to read data!");
                System.exit(1);
            } catch (NumberFormatException e){
                System.err.println("please, write correct digit");
            }


        }
    }

}

